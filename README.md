# Exchanges

## Install

```
git clone git@gitlab.com:Papipo/exchanges.git
cd exchanges
bundle
```

## Usage

```
rake import
rake conversion[120,2011-03-05]
```

## Run tests

```
rake
```
