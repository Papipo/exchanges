require "sqlite3"

module Exchanges
  class << self
    attr_accessor :database_path

    def convert(amount, day)
      day = day.gsub("-", "").to_i
      amount = BigDecimal.new(amount)
      rate = BigDecimal.new(rate_for(day), 5)
      (amount / rate).round(2)
    end

    def db
      @db ||= create_database
    end

    protected

    def rate_for(day)
      db.get_first_value("SELECT rate FROM exchanges WHERE day = ?", day) ||
      db.get_first_value("SELECT rate FROM exchanges WHERE day < ? ORDER BY day DESC LIMIT 1", day)
    end

    def create_database
      database = SQLite3::Database.new(database_path || "exchanges.db")
      database.execute <<-SQL
        CREATE TABLE IF NOT EXISTS `exchanges` (
          `day` int(8) PRIMARY KEY,
          `rate` REAL
        );
      SQL
      database
    end
  end
end
