require "net/http"
require_relative "../exchanges"

module Exchanges
  module Importer
    class << self
      def run
        db.transaction do
          exchanges do |day, rate|
            insert(day, rate)
          end
        end
      end

      protected
      def fetch
        Net::HTTP.get_response(URI('http://sdw.ecb.europa.eu/quickviewexport.do?SERIES_KEY=120.EXR.D.USD.EUR.SP00.A&type=csv')).body
      end

      def exchanges(&block)
        fetch.lines.lazy.each do |line|
          day, rate = line.split(",")
          block.call(Integer(day.gsub("-", "")), Float(rate)) rescue ArgumentError
        end
      end

      def insert(day, rate)
        db.execute("INSERT INTO exchanges VALUES (?, ?)", [day, rate])
      end

      def db
        Exchanges.db
      end
    end
  end
end
