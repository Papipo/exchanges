require_relative "../test_helper"
require_relative "../../lib/exchanges/importer"

class ExchangesImporterTest < Minitest::Test
  include TestHelper

  def test_import
    delete_database
    assert_nil latest
    with_cassette do
      Exchanges::Importer.run
    end
    assert_equal 20170331, latest
  end

  protected
  def latest
    Exchanges.db.get_first_value("SELECT day FROM exchanges ORDER BY day DESC LIMIT 1")
  end
end
