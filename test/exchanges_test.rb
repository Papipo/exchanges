require_relative "test_helper"

class ExchangesTest < Minitest::Test
  include TestHelper

  def test_conversion
    import_database
    assert_equal 85.98, Exchanges.convert("120", "2011-03-04")
    assert_equal 85.98, Exchanges.convert("120", "2011-03-05")
  end
end
