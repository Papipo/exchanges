require "minitest/autorun"
require "vcr"
require_relative "../lib/exchanges"
require_relative "../lib/exchanges/importer"

VCR.configure do |config|
  config.cassette_library_dir = File.join(File.dirname(__FILE__), "vcr_cassettes")
  config.hook_into :webmock
end

Exchanges.database_path = File.join(File.dirname(__FILE__), "test.db")

module TestHelper
  def delete_database
    File.delete(Exchanges.database_path) rescue Errno::ENOENT
  end

  def import_database
    with_cassette do
      Exchanges::Importer.run
    end
  end

  def with_cassette(&block)
    VCR.use_cassette("csv", &block)
  end
end
